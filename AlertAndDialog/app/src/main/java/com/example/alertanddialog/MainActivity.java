package com.example.alertanddialog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickController(View view) {
        int id = view.getId();

        Intent intent;
        if(id == R.id.alert_button) {
            intent = new Intent(this, Alert.class);
        } else {
            intent = new Intent(this, Dialog.class);
        }

        startActivity(intent);
    }
}